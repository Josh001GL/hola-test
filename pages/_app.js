import '../styles/globals.css'
import Script from 'next/script'


/*
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-J24GHRCB6P"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-J24GHRCB6P');
</script>
*/

function MyApp({ Component, pageProps }) {
  return (
    <>
    <Script
    id="my-script"
      strategy="lazyOnload"
      async src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}`}

    />
     
    <Script
    id="my-script2"
      strategy="lazyOnload"
    >{`
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}');
    `}</Script>
     
     <Component {...pageProps} />

    </>
  )
}

export default MyApp


